// fetch(UR, {
//    method: "POST",
//    body: JSON.stringify(t),
//    headers: { "Content-Type": "application/json" },
//  }).then((reponse) => console.log(reponse));
const loader = document.querySelector(".loader");
const container = document.querySelector(".container");

async function getUsers() {
  const usersUrl = await fetch("https://ajax.test-danit.com/api/json/users");
  const data = await usersUrl.json();
  return data;
}
async function getPosts() {
  const postsUrl = await fetch("https://ajax.test-danit.com/api/json/posts");
  const data = await postsUrl.json();
  return data;
}

class Card {
  constructor(user, post) {
    this.user = user;
    this.post = post;
  }
  creareDOMElemetn() {
    const div = document.createElement("div");
    div.classList = "container-item";
    div.setAttribute("data-index", this.post.id);

    const h3 = document.createElement("h3");
    h3.textContent = this.user.name;

    let span = document.createElement("span");
    span.textContent = this.user.email;
    span.classList = "user-email";
    h3.appendChild(span);

    const hr = document.createElement("hr");

    const p = document.createElement("p");
    p.textContent = this.post.title;
    p.classList = "title";

    const p2 = document.createElement("p");
    p2.textContent = this.post.body;

    const button = document.createElement("button");
    button.textContent = "DELETE";
    button.classList = "btn-delete";
    div.append(h3, p, p2, button, hr);

    return div;
  }
}
async function reuestRender() {
  const [users, posts] = await Promise.all([getUsers(), getPosts()]);
  users.forEach((user) => {
    posts
      .filter((post) => user.id === post.userId)
      .forEach((post) => {
        const card = new Card(user, post);
        const cardElement = card.creareDOMElemetn();
        container.appendChild(cardElement);
        loader.remove();
      });
  });
}
reuestRender();

async function deleteCard(id) {
  await fetch(`https://ajax.test-danit.com/api/json/posts/${id}`, {
    method: "DELETE",
  }).then((response) => {
    if (response.ok) {
      let card = document.querySelector(`.container-item[data-index="${id}`);
      console.log(card);
      if (card) {
        card.remove();
      }
    }
  });
}

document.body.addEventListener("click", (event) => {
  if (event.target.classList.contains("btn-delete")) {
    deleteCard(event.target.parentElement.getAttribute("data-index"));
  }
});
